/*
 * Copyright (C) 2017 thtf, Inc. All Rights Reserved.
 */
package com.thtf.ocr.ui.camera;

/**
 * 权限回调
 */
public interface PermissionCallback {
    boolean onRequestPermission();
}
