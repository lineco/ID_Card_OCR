package com.tomcat.custom.util;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;

import java.io.IOException;

public class MediaPlayerManager {
    private MediaPlayer mPlayer;
    private int rawID;
    private Context context;
    private AssetFileDescriptor fileDescriptor;

    public MediaPlayerManager(Context context, int rawID) {
        this.context = context;
        this.rawID = rawID;
        initMediaPlayer();
    }

    public void setRawID(int rawID) {
        this.rawID = rawID;
        resetDataResource();
    }

    private void initMediaPlayer() {
        //1.获取模式
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        final int ringerMode = am.getRingerMode();
        if (ringerMode == AudioManager.RINGER_MODE_NORMAL) {
            try {
                mPlayer = new MediaPlayer();
                resetDataResource();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 重设播放文件资源
     */
    private void resetDataResource() {
        fileDescriptor = context.getResources().openRawResourceFd(rawID);
        try {
            mPlayer.setDataSource(fileDescriptor.getFileDescriptor(), fileDescriptor.getStartOffset(), fileDescriptor.getLength());
            fileDescriptor.close();
        } catch (IOException e) {
            e.printStackTrace();
            mPlayer.reset();
        }
    }

    /**
     * 准备
     */
    private void prepareMedia() {
        if (mPlayer == null)
            return;
        try {
            mPlayer.prepare();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 播放
     */
    public void playerMedia() {
        if (mPlayer == null)
            return;
        if (!mPlayer.isPlaying()) {
            prepareMedia();
            mPlayer.start();
        }
    }

    public void albumMedia() {
        Uri systemAudioUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone rt = RingtoneManager.getRingtone(context, systemAudioUri);
        rt.play();
    }
    /**
     * 结束播放来电和呼出铃声
     */
    public  void stopMedia() {
        if (mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.stop();
            mPlayer.release();
        }
        mPlayer = null;
    }

}
